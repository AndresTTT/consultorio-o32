async function loadData() {
    let response = await sendRequest("api/citas", "GET", "");
    let table = document.getElementById("citas-table");
    table.innerHTML = "";
    let data = await response;
    data.forEach((element) => {
      table.innerHTML += `
                  <tr>
                      <th>${element.id}</th>
                      <td>${element.fechaCita}</td>
                      <td>${element.especialidad}</td>
                      <td>${element.doctor}</td>
                      <td>${element.indicaciones}</td>
                      <td>${element.estado}</td>
                      <td>${element.createdAt}</td>
                      <td>${element.updateAt ? element.updateAt : ""}</td>
                      <td>
                          <button type="button" class="btn btn-primary" onclick='window.location = "guardarcita.html?id=${
                            element.id
                          }"'>Editar</button>
                          <button type="button" class="btn btn-danger" onclick='borrarCita(${
                            element.id
                          })'>Eliminar</button>
                      </td>
                  </tr>
  
                  `;
    });
  }
  
  async function listarCitas(idCita) {
    let response = await sendRequest("api/citas" + idCita, "GET", "");
    let id = document.getElementById("id");
  
    let data = await response;
    let citas = data;
    id.value = citas.id;
    
  }
  
  async function borrarCita(id) {
    try {
      await sendRequest("api/citas/{id}" + id + "/remove", "DELETE", "");
      loadData();
    } catch (error) {
      console.error(error.toString());
    }
  }
  
  async function guardarCita() {
    try {
      let id = document.getElementById("id").value;
      let data = {
        id: id,
        fechaCita: fechaCita,
        doctor: doctor,
        indicaciones: indicaciones,
        estado: estado,
      };
      let response = id
        ? await sendRequest("api/citas" + id, "PUT", data)
        : await sendRequest("citas", "POST", data);
      window.location = "citas.html";
      console.log(response);
    } catch (error) { console.error(error.toString());}
  }
  