async function loadData() {
  let response = await sendRequest("api/medicamentos", "GET", "");
  let table = document.getElementById("medicamentos-table");
  table.innerHTML = "";
  let data = await response;
  data.forEach((element) => {
    table.innerHTML += `
                <tr>
                    <th>${element.id}</th>
                    <td>${element.nombre}</td>
                    <td>${element.cantidad}</td>
                    <td>${element.indicaciones}</td>
                    <td>${element.createdAt}</td>
                    <td>${element.updateAt ? element.updateAt : ""}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "guardarmedicamento.html?id=${
                          element.id
                        }"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='borrarMedicamento(${
                          element.id
                        })'>Eliminar</button>
                    </td>
                </tr>

                `;
  });
}

async function listarMedicamento(idMedicamento) {
  let response = await sendRequest("api/medicamentos" + idMedicamento, "GET", "");
  let id = document.getElementById("id");

  let data = await response;
  let medicamento = data;
  id.value = medicamento.id;
  
}

async function borrarMedicamento(id) {
  try {
    await sendRequest("api/medicamentos/{id}" + id + "/remove", "DELETE", "");
    loadData();
  } catch (error) {
    console.error(error.toString());
  }
}

async function guardarMedicamento() {
  try {
    let id = document.getElementById("id").value;
    let data = {
      id: id,
      nombre: nombre,
      cantidad: cantidad,
      indicaciones: indicaciones,
    };
    let response = id
      ? await sendRequest("api/medicamentos" + id, "POST", data)
      : await sendRequest("medicamento", "POST", data);
    window.location = "medicamento.html";
    console.log(response);
  } catch (error) { console.error(error.toString());}
}
