package com.misiontic.consultorio_o32.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.consultorio_o32.daos.MedicamentoDao;
import com.misiontic.consultorio_o32.modelos.Medicamentos;


@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ControladorMedicamento {
    
    @Autowired
    MedicamentoDao medicamentoDao;

    
    @PostMapping("/medicamentos")
    public ResponseEntity<Medicamentos> guardarMedicamento(@RequestBody Medicamentos medicamento){

        Medicamentos nuevoMedicamento = medicamentoDao.save(new Medicamentos(medicamento.getNombre(), medicamento.getCantidad(), medicamento.getIndicaciones()));
        return new ResponseEntity<>(nuevoMedicamento, HttpStatus.CREATED);

    }


    @PutMapping("/medicamentos/{id}")
    public ResponseEntity<Medicamentos> actualizarMedicamento(@PathVariable("id") long id, @RequestBody Medicamentos medicamento){

        Optional<Medicamentos> medicamentoactualizar = medicamentoDao.findById(id);
        if(medicamentoactualizar.isPresent()){
             
            Medicamentos _medicamento = medicamentoactualizar.get();
            
            _medicamento.setNombre(medicamento.getNombre());
            _medicamento.setCantidad(medicamento.getCantidad());
            _medicamento.setIndicaciones(medicamento.getIndicaciones());

            return new ResponseEntity<>(medicamentoDao.save(_medicamento), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/medicamentos/{id}")
    public ResponseEntity<Medicamentos> listarMedicamento(@PathVariable("id") long id){

        Optional<Medicamentos> medicamento = medicamentoDao.findById(id);
        
        if(medicamento.isPresent()){

            return new ResponseEntity<>(medicamento.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @GetMapping("/medicamentos")
    public ResponseEntity<List<Medicamentos>> listarMedicamentos(){

        List<Medicamentos> medicamentos = new ArrayList<Medicamentos>();
        medicamentoDao.findAll().forEach(medicamentos::add);

        if(medicamentos.isEmpty()){
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
        return  new ResponseEntity<>(medicamentos, HttpStatus.OK);
        }

    }


    @DeleteMapping("/medicamentos/{id}")
    public ResponseEntity<HttpStatus> borrarMedicamento(@PathVariable("id") long id){

        medicamentoDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}

