package com.misiontic.consultorio_o32.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.consultorio_o32.daos.CitaDao;
import com.misiontic.consultorio_o32.modelos.Citas;



@RestController
@RequestMapping("/api")
public class ControladorCita {
    
    @Autowired
    CitaDao citaDao;

    @PostMapping("/citas")
    public ResponseEntity<Citas> guardarCita(@RequestBody Citas cita){

        Citas nuevaCita = citaDao.save(cita);
        return new ResponseEntity<>(nuevaCita, HttpStatus.CREATED);


    }

    @PutMapping("/citas/{id}")
    public ResponseEntity<Citas> actualizarCita(@PathVariable("id") long id, @RequestBody Citas cita){

        Optional<Citas> citaActualizar = citaDao.findById(id);
        if(citaActualizar.isPresent()){
             
            Citas _cita = citaActualizar.get();
            
            _cita.setDoctor((cita.getDoctor()));
            _cita.setFechaCita(cita.getFechaCita());
            _cita.setEspecialidad(cita.getEspecialidad());
            _cita.setIndicaciones(cita.getIndicaciones());
            _cita.setEstado(cita.getEstado());
            

            return new ResponseEntity<>(citaDao.save(_cita), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/citas")
    public ResponseEntity<List<Citas>> listarCitas(){

        List<Citas> cita = new ArrayList<Citas>();
        citaDao.findAll().forEach(cita::add);

        if(cita.isEmpty()){
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
        return  new ResponseEntity<>(cita, HttpStatus.OK);
        }

    }



    @GetMapping("/citas/{id}")
    public ResponseEntity<Citas> listarcita(@PathVariable("id") long id){

        Optional<Citas> cita = citaDao.findById(id);
        
        if(cita.isPresent()){

            return new ResponseEntity<>(cita.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




    @DeleteMapping("/citas/{id}")
    public ResponseEntity<HttpStatus> borrarcita(@PathVariable("id") long id){

        citaDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    

}

