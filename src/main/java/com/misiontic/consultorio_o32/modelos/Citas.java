package com.misiontic.consultorio_o32.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="citas")
public class Citas {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idCitas;

    @Column(name="especialidad", nullable = false)
    private String especialidad;

    @Column(name="fechacita", nullable = false)
    private String fechaCita;

    @Column(name="doctor", nullable = false)
    private String doctor;

    @Column(name="indicaciones", nullable = false)
    private String indicaciones;

    @Column(name = "estado")
    private String estado;

    public Citas() {
    }

    public Citas(String especialidad, String fechaCita, String doctor, String indicaciones, String estado) {
        this.especialidad = especialidad;
        this.fechaCita = fechaCita;
        this.doctor = doctor;
        this.indicaciones = indicaciones;
        this.estado = estado;
    }

    public long getIdCitas() {
        return idCitas;
    }

    public void setIdCitas(long idCitas) {
        this.idCitas = idCitas;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Citas [doctor=" + doctor + ", especialidad=" + especialidad + ", estado=" + estado + ", fechaCita="
                + fechaCita + ", idCitas=" + idCitas + ", indicaciones=" + indicaciones + "]";
    }

}
