package com.misiontic.consultorio_o32.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="medicamentos")
public class Medicamentos {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMedicamentos;

    @Column(name="nombre", nullable = false)
    private String nombre;

    @Column(name="cantidad", nullable = false)
    private int cantidad;

    @Column(name="indicaciones", nullable = false)
    private String indicaciones;

    @ManyToOne
    @JoinColumn(name = "idUsuario")
    private Usuario usuario;

    public Medicamentos() {
    }

    public Medicamentos(String nombre, int cantidad, String indicaciones) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.indicaciones = indicaciones;
    }

    public long getIdMedicamentos() {
        return idMedicamentos;
    }

    public void setIdTMedicamentos(long idMedicamentos) {
        this.idMedicamentos = idMedicamentos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Medicamentos [cantidad=" + cantidad + ", idMedicamentos=" + idMedicamentos + ", indicaciones="
                + indicaciones + ", nombre=" + nombre + ", usuario=" + usuario + "]";
    }

}
