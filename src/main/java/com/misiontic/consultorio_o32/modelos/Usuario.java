package com.misiontic.consultorio_o32.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idUsuario;

    @Column(name = "tipodocumento", nullable = false)
    private String tipoDocumento;

    @Column(name = "documento", nullable = false)
    private int documento;

    @Column(name = "nombre", nullable = false)
    private String nombre;
    
    @Column(name = "username", nullable = false, unique = true)
    private String userName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "clave", nullable = false)
    private String clave;

    @ManyToOne
    @JoinColumn(name = "idRol")
    private Roles roles;

    public Usuario() {
    }

    public Usuario(String tipoDocumento, int documento, String nombre, String userName, String email, String clave) {
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
        this.nombre = nombre;
        this.userName = userName;
        this.email = email;
        this.clave = clave;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Usuario [clave=" + clave + ", documento=" + documento + ", email=" + email + ", idUsuario=" + idUsuario
                + ", nombre=" + nombre + ", roles=" + roles + ", tipoDocumento=" + tipoDocumento + ", userName="
                + userName + "]";
    }

}
