package com.misiontic.consultorio_o32.servicios;

import java.util.List;

import com.misiontic.consultorio_o32.modelos.Citas;


public interface ServicioCita {

    public Citas save(Citas cita);

    public List<Citas> listaTodos();
}

