package com.misiontic.consultorio_o32.servicios;

import java.util.List;

import com.misiontic.consultorio_o32.modelos.Medicamentos;

public interface ServicioMedicamento {

    public Medicamentos save(Medicamentos medicamento);

    public List<Medicamentos> listaTodos();
}
