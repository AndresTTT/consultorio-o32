package com.misiontic.consultorio_o32.servicios.implementacion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.misiontic.consultorio_o32.modelos.Medicamentos;
import com.misiontic.consultorio_o32.daos.MedicamentoDao;
import com.misiontic.consultorio_o32.servicios.ServicioMedicamento;



public class MedicamentoServiciolmpl implements ServicioMedicamento {
    

    @Autowired
    MedicamentoDao medicamentoDao;

    public Medicamentos save(Medicamentos medicamento){

        Medicamentos nuevoMedicamento = medicamentoDao.save(new Medicamentos(medicamento.getNombre(), medicamento.getCantidad(), medicamento.getIndicaciones()));
        return nuevoMedicamento;

    }


    public List<Medicamentos> listaTodos(){

        List<Medicamentos> medicamentos = new ArrayList<Medicamentos>();
        medicamentoDao.findAll().forEach(medicamentos::add);

        if(!medicamentos.isEmpty()){
      
        return medicamentos;
        }else{
            return medicamentos;
        }


        
    }

    
}

