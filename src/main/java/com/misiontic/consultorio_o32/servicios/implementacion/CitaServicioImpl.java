package com.misiontic.consultorio_o32.servicios.implementacion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.misiontic.consultorio_o32.daos.CitaDao;
import com.misiontic.consultorio_o32.modelos.Citas;
import com.misiontic.consultorio_o32.servicios.ServicioCita;

public class CitaServicioImpl implements ServicioCita {
    

    @Autowired
    CitaDao citaDao;

    public Citas save(Citas cita){

        Citas nuevaCita = citaDao.save(new Citas(cita.getEspecialidad(), cita.getFechaCita(), cita.getDoctor(), cita.getIndicaciones(), cita.getEstado()));
        return nuevaCita;

    }


    public List<Citas> listaTodos(){

        List<Citas> citas = new ArrayList<Citas>();
        citaDao.findAll().forEach(citas::add);

        if(!citas.isEmpty()){
      
        return  citas;
        }else{
            return  citas;
        }


        
    }

    
}
