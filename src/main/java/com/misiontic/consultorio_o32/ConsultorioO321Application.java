package com.misiontic.consultorio_o32;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultorioO321Application {

	public static void main(String[] args) {
		SpringApplication.run(ConsultorioO321Application.class, args);
	}

}
