package com.misiontic.consultorio_o32.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.consultorio_o32.modelos.Citas;

public interface CitaDao extends JpaRepository<Citas, Long> {

   
}