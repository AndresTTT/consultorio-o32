package com.misiontic.consultorio_o32.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.consultorio_o32.modelos.Usuario;

public interface UsuarioDao extends JpaRepository<Usuario, Long> {

    public Usuario findByUserName(String userName);
}
